const path = require('path');   
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const CssMinimizer = require("optimize-css-assets-webpack-plugin");
const autoprefixer = require('autoprefixer');
const RemovePlugin = require('remove-files-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require("webpack");
const config = {
    devtool: 'inline-source-map',
    entry: {
        "js/main":'./src/index.js',
        "css/main":'./src/scss/main.scss',
        "css/480":'./src/scss/480.scss',
        "css/768":'./src/scss/768.scss',
        "css/1200":'./src/scss/1200.scss'
    },
    output: {      
        path: path.resolve(process.cwd(), 'dist'),
        filename: "[name].js",
        publicPath: 'dist/'
    },
    optimization: {

    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        lazy: true,
    },
    module: {
        rules: [
           {
                test: /\.js$/,
                include: path.resolve(__dirname, 'src/js'),
                use: {
                    loader : "babel-loader",
                    options : {
                       "presets":
                        [
                            [
                              "env",
                              {
                                "loose": true,
                                "modules": false
                              }
                            ]
                        ]
                    }
                }
            },
            { 
                test:  /\.(sa|sc|c)ss$/,
                include: path.resolve(__dirname, 'src/scss'),
                use: 
                [
                    {
                        loader: MiniExtractPlugin.loader
                    },
                    {
                        loader: "css-loader",
                        options: {url: false}
                    },
                    {
                    loader: 'postcss-loader',
                    options: {
                        plugins: [
                            autoprefixer({
                                browsers:['ie >= 8', 'last 4 version']
                            })
                        ],
                        sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader",
                    }
                ]
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
          $: 'jquery',
          jQuery: 'jquery'
        }),
        new UglifyJsPlugin({
              test: /\.js(\?.*)?$/i,
                uglifyOptions: {
                    output: {
                        comments: false,
                    },
                },
        }),
        new CopyPlugin([               
            {from:'src/img',to:'img'}
        ]),
        new ImageminPlugin([     
            { 
                test: /\.(jpe?g|png|gif|svg)$/i,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]', 
                    context: ''
                }
            }
        ]), 
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            filename: 'index.html',
            inject: false
        }),
        new CssMinimizer({
            cssProcessor: require("cssnano"),
            cssProcessorOptions: {
            discardComments: {
                removeAll: true
            },
            safe: true
            },
                canPrint: false,
            }),
        new MiniExtractPlugin({
            filename: '[name].min.css',
            allChunks: true,
        }),
        new RemovePlugin({
            before: {
               include: ['dist']
            },
            after: {
                test: [
                {
                    folder: 'dist/css',
                    method: (filePath) => {
                        return new RegExp(/\.js$/, 'm').test(filePath);
                    },
                    recursive: true
                }
                ]
            }
        }),
    ],
};
module.exports = config;
